﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Slave : CharacterCore {
	public Vector3 destination = new Vector3(-1, -1);
	private float wanderSpeed = 0.05f;
	public bool wandering = true;
	private bool stuck = false;
	private float mustWanderTime = 0; 
	public int resourcesInPosession = 0;
	public List<GameObject> resourceTrail = new List<GameObject>();

	private float safetyTimer = 10;
	private ResourceSpawner targetSpawner;

	public bool IsAvailable() {
		return wandering && mustWanderTime < 0.02f && targetSpawner == null;
	}

	public override void Start() {
		base.Start();
		if (destination.x < 0 && destination.y < 0) {
			// If a destination was assigned before we got initialized, don't overwrite it
			destination = transform.position;
		}
		myDamageSound = minionDamageSound;
		myDeathSound = minionDeathSound;
	}

	public override void OnDestroy() {
		base.OnDestroy();
		foreach(GameObject resource in resourceTrail) {
			Object.Destroy(resource);
		}
	}

	public override void Update () {
		base.Update();

		Vector3 lastPosition = transform.position;

		if(mustWanderTime > 0) {
			mustWanderTime -= Time.deltaTime;
			if(mustWanderTime < 0) {
				mustWanderTime = 0;
				if(resourcesInPosession > 0 && Random.value < 0.5f) {
					SetDestination(GameObject.FindGameObjectWithTag("ResourceConverter").transform.position);
				}
			}
		}


		if(destination != transform.position) {
			Vector3 direction = destination - transform.position;
			direction.Normalize();
			if(wandering) {
				direction *= wanderSpeed;
				// go faster when getting unstuck
				if(mustWanderTime > 0) {
					direction *= (1.0f + maxSpeed * mustWanderTime);
				}
			}
			if(direction.magnitude > (destination - transform.position).magnitude) {
				direction = (destination - transform.position);
			}
			MovementControl(direction);
			if((transform.position - destination).magnitude < 0.01) {
				transform.position = destination;
				wandering = true;
			}
		} else {
			Vector3 targetPosition;
			float range = (mustWanderTime > 0) ? 2.0f : 0.5f;
			do {
				targetPosition = new Vector3(
					Random.Range(transform.position.x - range, transform.position.x + range),
					Random.Range(transform.position.y - range, transform.position.y + range),
					0);
			} while(!TileIsWalkable(targetPosition));
			SetDestination(targetPosition);
		}

		for(int i = 0; i < resourceTrail.Count; i++) {
			GameObject resource = resourceTrail[i];
			Vector3 distance = transform.position - resource.transform.position;
			if(distance.magnitude > 0.5f * (i + 1)) {
				if((distance.normalized / 10.0f).magnitude > 0.5f * (i + 1)) {
					resource.transform.Translate(distance.normalized * 0.5f * (i + 1));
				} else {
					resource.transform.Translate(distance.normalized / 10.0f);
				}
			}
		}

		if(!wandering && (transform.position - lastPosition).magnitude < Time.deltaTime) {
			// If the slave didn't move very far this update, it's stuck.
			OnMapCollisionTriggered();
		}
	}

	protected override void OnMapCollisionTriggered() {
		safetyTimer -= Time.deltaTime * 60;
		if (safetyTimer <= 0) {
			safetyTimer = 10;
			if(stuck && targetSpawner == null) {
				ReleaseFromTarget();
			} else {
				stuck = true;
			}
		}
	}

	public bool IsStuck() {
		return stuck;
	}

	public void TargetSpawner(ResourceSpawner spawner) {
		targetSpawner = spawner;
		SetDestination(spawner.transform.position);
		wandering = false;
	}

	public void ReleaseFromTarget() {
		if(targetSpawner != null) {
			targetSpawner.ReleaseSlave(stuck);
			targetSpawner = null;
		}
		stuck = false;
		wandering = true;
		mustWanderTime = 1.0f;
		destination = transform.position;
	}

	public void WasSelected() {
		PlaySound(questionSound);
	}

	public void SetDestination(Vector3 target, bool userInput = false) {
		destination.z = 0;
		destination = target;
		if(userInput) {
			PlaySound(confirmSound, 0.75f, 1.1f);
			wandering = false;
		} else {
			wandering = true;
		}
	}

	public void AddResource(GameObject resource) {
		resource.transform.position = transform.position + new Vector3(Random.Range(-2f, 2f), Random.Range(-2f, 2f));
		resource.SetActive(true);
		resourceTrail.Add(resource);

		PlaySound(pickupSound, 0.8f, 1.1f);
		resourcesInPosession++;
		SetDestination(GameObject.FindGameObjectWithTag("ResourceConverter").transform.position);
		targetSpawner = null;
		wandering = false;
	}

	public static Slave NearestWanderingSlave (Vector3 position)
	{
		string tag = "Slave";
		Slave characters = null;
		GameObject[] objects = GameObject.FindGameObjectsWithTag (tag);
		foreach (GameObject obj in objects) {
			Slave core = obj.GetComponent<Slave> ();
			if(core == null) continue;

			if (core.IsAvailable() && (characters == null || Vector3.Distance(core.transform.position, position) < Vector3.Distance(characters.transform.position, position))) {
				characters = core;
			}
		}

		return characters;
	}
}
