﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour {

	public float MAX_SPEED_X = 7;
	public float MAX_SPEED_Y = 8;
	public float OFFSET_X = 0;
	public float OFFSET_Y = 0;
	public float LERP_PERCENT_X = 0.06f; 
	public float LERP_PERCENT_Y = 0.2f;
	public float LERP_PERCENT_X2 = 0.5f;
	public float LERP_PERCENT_Y2 = 0.6f;
	public float CENTER_WIDTH = 0;
	public float CENTER_HEIGHT = 0;

	private bool initializeCamera;

	private float px = 0f;
	private float py = 0f;
	//private Vector2 target;
	//private Vector2 current_speed = new Vector2(0, 0);

	void Start() {
		initializeCamera = true;
	}

	void Update() {
		Vector3 bottomLeft = GetComponent<Camera>().ScreenToWorldPoint (new Vector3 (0, 0, -transform.position.z));
		Vector3 topRight = GetComponent<Camera>().ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, -transform.position.z));
		CENTER_WIDTH = (topRight.x - bottomLeft.x) / 2;
		CENTER_HEIGHT = (topRight.y - bottomLeft.y) / 2;
		if(initializeCamera) {
			initializeCamera = false;
			transform.position = new Vector3(CENTER_WIDTH, CENTER_HEIGHT, transform.position.z);
			//target = new Vector2 (transform.position.x, transform.position.y);
		}
	}

	public void DoFollow(Vector3 Obj_Player, Rect constraints) {
		px = Obj_Player.x;
		py = Obj_Player.y;
		//int pDir = 1;

		float targetX = Mathf.Clamp(px, constraints.xMin + CENTER_WIDTH, constraints.xMax - CENTER_WIDTH);
		float targetY = Mathf.Clamp(py, constraints.yMin + CENTER_HEIGHT, constraints.yMax - CENTER_HEIGHT);

		transform.position = new Vector3 (targetX, targetY, transform.position.z);
	}
}
