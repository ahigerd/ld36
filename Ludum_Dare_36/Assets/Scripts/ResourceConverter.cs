﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResourceConverter : MonoBehaviour {

	private int resources = 0;
	private List<GameObject> resourcePile = new List<GameObject>();

	private void PurgeResources() {
		resources = 0;
		foreach(GameObject resource in resourcePile) {
			Object.Destroy(resource);
		}
		resourcePile.Clear();
	}

	void OnDestroy() {
		PurgeResources();
	}

	void FixedUpdate () {
		CharacterCore[] characters;

		characters = CharacterCore.CollisionCheck (CharacterCore.RectToWorldSpace(transform, new Rect (-1.5f, -1.5f, 3, 3)), "Slave");

		foreach (CharacterCore character in characters) {
			Slave slave = (Slave)character;

			if (slave.resourcesInPosession > 0) {
				PlayerControl.IncreaseScore (10 * slave.resourcesInPosession);
				resources += slave.resourcesInPosession;
				slave.resourcesInPosession = 0;
				foreach(GameObject resource in slave.resourceTrail) {
					resource.transform.position = new Vector3(
						Random.Range(transform.position.x - 1.25f, transform.position.x + 1.25f),
						Random.Range(transform.position.y - 1.75f, transform.position.y + 0.75f),
						0);
					resourcePile.Add(resource);
				}
				slave.resourceTrail = new List<GameObject>();
				slave.wandering = true;
				slave.RestoreHealth (100);
			}
		}

		if (resources > 0) {
			characters = CharacterCore.CollisionCheck (CharacterCore.RectToWorldSpace(transform, new Rect (-1.5f, -1.5f, 3, 3)), "Player");
			if (characters.Length == 0)
				return;

			characters [0].RestoreHealth(resources*5);
			PurgeResources();
		}
	}
}
