using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraControl : MonoBehaviour {

	public enum CameraMode {
		OverviewMode,
		FollowMode
	};

	//  Public member variables
	public int score = 0;
	public float cameraSpeed = 10;
	public Texture2D tileSheet;
	public GameObject backgroundRenderer;
	public float slaveSelectionRadius = 1;
	public Transform playerPrefab;
	public Transform enemyPrefab;
	public Transform slavePrefab;
	public Transform pointerPrefab;
	public Transform arrowPrefab;
	public CameraMode mode = CameraMode.FollowMode;
	public static bool showScores = true;

	public GameObject resourceSpawnerPrefab;
	public List<ResourceSpawner> resourceSpawners;

	public GameObject resourceConverterPrefab;
	public ResourceConverter resourceConverter;

	//  private member variables
	private Rect boundingBox = new Rect(0, 0, 100, 98);
	private TileMap map = null;
	private Sprite background = null;
	private Transform player;
	private Transform selected;
	private int numberOfEnemiesAtStart = 2;
	private List<Transform> enemies;

	private Texture2D titleImage;

	//  camera control

	void Start() {
		backgroundRenderer.GetComponent<SpriteRenderer>().sortingOrder = -32768;
		pointerPrefab.GetComponent<SpriteRenderer>().sortingOrder = 32767;
		arrowPrefab.GetComponent<SpriteRenderer>().sortingOrder = 32767;
		NewLevel();
		showScores = true;

		titleImage = Resources.Load("Images/title") as Texture2D;
	}

	Vector2 FindSpawnPoint(Rect range, Vector2 avoid = new Vector2(), int avoidRange = 10) {
		return map.FindOpenTile(range, avoid, avoidRange);
	}

	void NewLevel() {
		Debug.Log("new level requested");

		// Clear out old level's objects
		if(background != null) {
			Object.Destroy(background);
		}
		if(player != null && player.gameObject != null) {
			Object.Destroy(player.gameObject);
		}
		foreach(GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy")) {
			Object.Destroy(enemy);
		}
		foreach(GameObject slave in GameObject.FindGameObjectsWithTag("Slave")) {
			Object.Destroy(slave);
		}
		if(resourceSpawners != null) {
			foreach(ResourceSpawner spawner in resourceSpawners) {
				Object.Destroy(spawner.gameObject);
			}
		}
		if(resourceConverter != null) {
			Object.Destroy(resourceConverter.gameObject);
		}

		map = new TileMap(tileSheet, (uint)System.DateTime.Now.ToBinary());
		background = map.RenderMap(50, 50);
		CharacterCore.map = map;
		resourceSpawners = new List<ResourceSpawner>();

		foreach(Vector2 spawnerPos in map.resourceSpawners) {
			GameObject spawnerObject = GameObject.Instantiate(resourceSpawnerPrefab) as GameObject;
			ResourceSpawner spawner = spawnerObject.GetComponent<ResourceSpawner>();
			spawner.transform.position = new Vector3(spawnerPos.x, 100 - spawnerPos.y);
			resourceSpawners.Add(spawner);
		}

		GameObject converterObject = GameObject.Instantiate(resourceConverterPrefab) as GameObject;
		resourceConverter = converterObject.GetComponent<ResourceConverter>();
		resourceConverter.GetComponent<SpriteRenderer>().sortingOrder = 32767;
		resourceConverter.transform.position = new Vector3(map.resourceConverter.x, 100 - map.resourceConverter.y);

		// Create player
		player = (Transform)Transform.Instantiate(
			playerPrefab,
			FindSpawnPoint(new Rect(20, 20, 60, 60)),
			Quaternion.identity);

		// Create enemies
		enemies = new List<Transform> ();
		EnemyController.killCounter = 1;
		for(int i = 0; i < numberOfEnemiesAtStart; i++) {
			Vector2 spawnpoint = FindSpawnPoint(new Rect(10, 10, 80, 80), player.transform.position);
			enemies.Add((Transform)Transform.Instantiate(enemyPrefab, spawnpoint, Quaternion.identity));
		}

		// Create minions
		Rect slaveRect = new Rect(player.transform.position.x - 8, player.transform.position.y - 4, 16, 8);
		for(int i = 0; i < 4; i++) {
			Vector2 spawnpoint = FindSpawnPoint(slaveRect, player.transform.position, 1);
			Transform.Instantiate(slavePrefab, spawnpoint, Quaternion.identity);
		}

		// Maps are huge textures. Let's free up some memory.
		Resources.UnloadUnusedAssets();
		System.GC.Collect();
	}

	void Update() {
		score = PlayerControl.score;

		if (showScores) {
			if (Input.anyKeyDown) {
				NewLevel ();
				showScores = false;
			}
		}

		Vector3 bottomLeft = GetComponent<Camera>().ScreenToWorldPoint (new Vector3 (0, 0, -transform.position.z));
		Vector3 topRight = GetComponent<Camera>().ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, -transform.position.z));
		Rect screenRect = new Rect(bottomLeft, topRight - bottomLeft);
		Plane leftPlane = new Plane(Vector3.right, bottomLeft);
		Plane bottomPlane = new Plane(Vector3.up, bottomLeft);
		Plane rightPlane = new Plane(Vector3.left, topRight);
		Plane topPlane = new Plane(Vector3.down, topRight);

		if (Input.GetMouseButtonDown (0)) {
			Vector2 pos = GetMousePositionAsWorldPosition();

			Vector2 slavePos = new Vector2 ();
			Transform nearestSlave = FindNearest("Slave", pos, ref slavePos);

			if (Vector2.Distance (pos, slavePos) < slaveSelectionRadius) {
				selected = nearestSlave;
				Slave actor = selected.gameObject.GetComponent<Slave>();
				actor.WasSelected();
			} else {
				selected = null;
				Debug.Log(map.GetTileType(new Vector2(pos.x, 100 - pos.y)));
			}
		} else if(Input.GetMouseButtonDown(1) && selected) {
			Slave actor = selected.gameObject.GetComponent<Slave>();
			actor.SetDestination(GetMousePositionAsWorldPosition(), true);
		}

		foreach(GameObject slaveObject in GameObject.FindGameObjectsWithTag("Slave")) {
			Slave slave = slaveObject.GetComponent<Slave>();
			if(slave.InPain()) {
				if(slave.damageArrow == null) {
					slave.damageArrow = GameObject.Instantiate(arrowPrefab).gameObject;
					slave.damageArrow.GetComponent<SpriteRenderer>().color = CharacterCore.ColorForHealth(slave.health/slave.maxHealth);
				} else {
					Color color = slave.damageArrow.GetComponent<SpriteRenderer>().color;
					color.a -= Time.deltaTime;
					slave.damageArrow.GetComponent<SpriteRenderer>().color = color;
				}
				slave.damageArrow.transform.eulerAngles = new Vector3(0, 0, 45);
				Vector3 direction = (slave.transform.position - transform.position);
				direction.z = 0;
				float angle = Mathf.Rad2Deg * Mathf.Atan2(direction.y, direction.x);
				slave.damageArrow.transform.RotateAround(Vector3.zero, Vector3.forward, angle + 180);

				Ray ray = new Ray(transform.position, direction);
				float edgeDist = 0;
				if(!leftPlane.Raycast(ray, out edgeDist)) {
					rightPlane.Raycast(ray, out edgeDist);
				}
				Vector3 point = ray.GetPoint(edgeDist - 0.8f);
				if(!screenRect.Contains(point)) {
					if(!topPlane.Raycast(ray, out edgeDist)) {
						bottomPlane.Raycast(ray, out edgeDist);
					}
				}
				point = ray.GetPoint(edgeDist - 0.8f);
				point.z = -5;
				slave.damageArrow.transform.position = point;
				slave.damageArrow.GetComponent<SpriteRenderer>().enabled =
					!screenRect.Contains(slave.transform.position);
			} else if(slave.damageArrow != null) {
				GameObject.Destroy(slave.damageArrow);
				slave.damageArrow = null;
			}
		}
	}

	void FixedUpdate () {
		if (enemies == null || enemies.Count == 0) {
			return;
		}

		for (int i = 0; i < enemies.Count; i++) {
			if (enemies [i] == null) {
				Vector2 spawnpoint = FindSpawnPoint (new Rect (10, 10, 80, 80), player.transform.position);
				enemies [i] = (Transform)Transform.Instantiate (enemyPrefab, spawnpoint, Quaternion.identity);
			}
		}

		if (EnemyController.killCounter == enemies.Count) {
			Vector2 spawnpoint = FindSpawnPoint(new Rect(10, 10, 80, 80), player.transform.position);
			enemies.Add((Transform)Transform.Instantiate(enemyPrefab, spawnpoint, Quaternion.identity));
			EnemyController.killCounter = 1;
			PlayerControl.IncreaseScore (150 * enemies.Count);
		}

	}

	void LateUpdate() {
		RunCameraStateMachine();

		foreach(CharacterCore character in CharacterCore.allCharacters) {
			character.GetComponent<SpriteRenderer>().sortingOrder = Mathf.FloorToInt(
				Mathf.Clamp((-character.transform.position.y * 600) + 30000, -32768, 32767));
		}

		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
		for(int i = 0; i < enemies.Length - 1; i++) {
			EnemyController enemy = enemies[i].GetComponent<EnemyController>();
			Rect enemyRect = CharacterCore.RectToWorldSpace(enemy.transform, enemy.hitBox);
			for(int j = i + 1; j < enemies.Length; j++) {
				EnemyController other = enemies[j].GetComponent<EnemyController>();
				Rect otherRect = CharacterCore.RectToWorldSpace(other.transform, other.hitBox);
				if(enemyRect.Overlaps(otherRect)) {
					Vector3 rebound = (enemy.transform.position - other.transform.position).normalized;
					enemy.transform.position += rebound / 20;
					other.transform.position -= rebound / 20;
				}
			}
		}

		backgroundRenderer.GetComponent<SpriteRenderer>().sprite = background;
		if(selected) {
			pointerPrefab.GetComponent<SpriteRenderer>().enabled = true;
			pointerPrefab.position = selected.position;
			pointerPrefab.Translate(new Vector3(0, 0.8f + Mathf.Abs(Mathf.Sin(Time.time * 4) * 0.2f), -0.5f));
		} else {
			pointerPrefab.GetComponent<SpriteRenderer>().enabled = false;
		}
	}

	private void RunCameraStateMachine() {
		switch (mode) {
		case CameraMode.OverviewMode:
			mode = OverviewMode ();
			break;
		case CameraMode.FollowMode:
			mode = FollowMode ();
			break;
		default:
			mode = FollowMode ();
			break;
		}
	}

	private CameraMode FollowMode(){
		if(background == null) {
			return CameraMode.FollowMode;
		}
		Bounds bounds = background.bounds;
		Rect rect = new Rect (bounds.min.x, bounds.min.y, bounds.max.x - bounds.min.x, bounds.max.y - bounds.min.y);
		//Debug.Log (bounds);
		if (player != null)
			GetComponent<FollowTarget>().DoFollow (player.position, rect);
		return CameraMode.FollowMode;
	}

	private CameraMode OverviewMode()
	{
		CamMove ();
		return CameraMode.OverviewMode;
	}

	private void CamMove(){
		float inputX = Input.GetAxis ("Horizontal");
		float inputY = Input.GetAxis ("Vertical");

		float deltaX = inputX * cameraSpeed * Time.deltaTime;
		float deltaY = inputY * cameraSpeed * Time.deltaTime;

		transform.Translate(deltaX, deltaY, 0);

		Vector3 bottomLeft = GetComponent<Camera>().ScreenToWorldPoint (new Vector3 (0, 0, -transform.position.z));
		Vector3 topRight = GetComponent<Camera>().ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, -transform.position.z));
		float CENTER_WIDTH = (topRight.x - bottomLeft.x) / 2;
		float CENTER_HEIGHT = (topRight.y - bottomLeft.y) / 2;

		float clampX = Mathf.Clamp(transform.position.x, boundingBox.xMin + CENTER_WIDTH, boundingBox.xMax - CENTER_WIDTH);
		float clampY = Mathf.Clamp(transform.position.y, boundingBox.yMin + CENTER_HEIGHT, boundingBox.yMax - CENTER_HEIGHT);

		transform.position = new Vector3 (clampX, clampY, transform.position.z);
	}

	private Vector2 GetMousePositionAsWorldPosition() {
		Camera camera = GetComponent<Camera>();
		Vector2 mousePos = Input.mousePosition;
		Vector3 p = camera.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, camera.nearClipPlane));
		return new Vector2 (p.x, p.y);
	}

	public static Transform FindNearest(string tag, Vector2 position)
	{
		Vector2 blah = new Vector2 ();
		return FindNearest (tag, position, ref blah);
	}

	public static Transform FindNearest(string tag, Vector2 position, ref Vector2 returnVector)
	{
		GameObject[] objects;
		objects = GameObject.FindGameObjectsWithTag (tag);
		if (objects.Length == 0)
			return null;
		Transform toReturn = objects[0].transform;
		Vector2 testVector = new Vector2 (position.x, position.y);
		returnVector = new Vector2 (objects[0].transform.position.x, objects[0].transform.position.y);

		foreach (GameObject obj in objects) {
			Vector2 currentVector = new Vector2 (obj.transform.position.x, obj.transform.position.y);
			if (Vector2.Distance (testVector, currentVector) < Vector2.Distance (testVector, returnVector)) {
				toReturn = obj.transform;
				returnVector = currentVector;
			}
		}

		return toReturn;
	}

	public void OnGUI() {
		GUIStyle boxStyle = new GUIStyle(GUI.skin.box);
		boxStyle.fontSize = 24;
		boxStyle.alignment = TextAnchor.MiddleRight;

		GUIStyle labelStyle = new GUIStyle(GUI.skin.label);
		labelStyle.fontSize = 32;
		labelStyle.alignment = TextAnchor.MiddleCenter;

		GUIStyle listStyle = new GUIStyle(labelStyle);
		listStyle.fontSize = 24;
		listStyle.alignment = TextAnchor.UpperLeft;

		if (!showScores) {
			Rect currentScore = new Rect(Screen.width - 200, 0, 200, 36);
			GUI.Box (currentScore, " ", boxStyle);
			GUI.Box (currentScore, "Score: " + score, boxStyle);
		}

		if (PlayerControl.paused) {
			Rect pauseBox = new Rect(Screen.width * 0.3f, Screen.height * 0.3f, Screen.width * 0.4f, Screen.height * 0.4f);
			GUI.Box (pauseBox, " ", boxStyle);
			GUI.Box (pauseBox, " ", boxStyle);
			if ((int)(System.DateTime.Now.Millisecond) < 500) {
				GUI.Label (pauseBox, "PAUSED\n\nPress Any Key", labelStyle);
			}
			return;
		}

		if (showScores) {
			Rect bg = new Rect (20, 20, Screen.width - ((Screen.width/3)*2), Screen.height - 40);
			Rect anyKey = new Rect (40 + (Screen.width - ((Screen.width/3)*2)), 20, ((Screen.width/3)*2) - 60, 50);

			GUI.Box (bg, " ");
			GUI.Box (bg, " ");

			GUI.Box (anyKey, " ");
			GUI.Box (anyKey, " ");
			if ((int)(System.DateTime.Now.Millisecond) < 500) {
				GUI.Label (anyKey, "Press Any Key to Start", labelStyle);
			}

			labelStyle.alignment = TextAnchor.UpperCenter;
			GUI.Label (bg, "HIGH SCORES", labelStyle);

			for (int i = 0; i < 10; i++) {
				int s = PlayerPrefs.GetInt ("score " + i, -1);
				if (s < 0)
					continue;
				GUI.Label (new Rect (40, 70 + (36 * i), Screen.width, Screen.height), "" + (i+1) + ":     " + s, listStyle);
			}

			GUI.DrawTexture(new Rect(40 + (Screen.width - ((Screen.width/3)*2)), 120, ((Screen.width/3)*2) - 60, Screen.height - 160), titleImage);
		}
	}
}
