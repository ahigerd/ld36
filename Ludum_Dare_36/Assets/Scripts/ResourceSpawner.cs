﻿using UnityEngine;
using System.Collections;

public class ResourceSpawner : MonoBehaviour {

	public float respawnDelay = 2000;

	private bool resourceReady = true;
	private float respawnCounter = 0;
	private float outOfReachCounter = 0;
	private int resourceType;
	public Slave slave = null;

	private SpriteAnimationManager manager;
	private GameObject pickupPrefab;

	// Use this for initialization
	void Start () {
		manager = GetComponent<SpriteAnimationManager>();
		GetComponent<SpriteRenderer>().sortingOrder = Mathf.FloorToInt(
			Mathf.Clamp((-transform.position.y * 600) + 30000, -32768, 32767));
		resourceType = Random.Range(0, 6);
		manager.SwitchAnimation(resourceType + 6, true);

		pickupPrefab = new GameObject("resource" + resourceType);
		SpriteRenderer pickupRenderer = pickupPrefab.AddComponent<SpriteRenderer>();
		pickupRenderer.sortingOrder = 32766;
		pickupRenderer.enabled = true;
		SpriteAnimationManager pickupManager = pickupPrefab.AddComponent<SpriteAnimationManager>();
		pickupManager.animations = manager.animations;
		pickupManager.pixelsPerUnit = manager.pixelsPerUnit;
		pickupManager.SwitchAnimation(resourceType, true);
		pickupManager.enabled = true;
	}

	public void ReleaseSlave(bool stuck) {
		slave = null;
		if(stuck)
			outOfReachCounter = respawnDelay;
	}

	void FixedUpdate () {
		//Debug.Log ("" + resourceReady + ",   " + slave);
		GetComponent<SpriteRenderer>().enabled = resourceReady;

		if (slave != null && slave.IsStuck()) {
			slave.ReleaseFromTarget();
		}

		if (resourceReady) {
			outOfReachCounter -= Time.deltaTime * 60f;
			if (outOfReachCounter <= 0) {

				if (slave == null) {
					slave = Slave.NearestWanderingSlave (transform.position);
					if (slave != null) {
						slave.TargetSpawner(this);
					}
				}
			}

			CharacterCore[] characters = CharacterCore.CollisionCheck (CharacterCore.RectToWorldSpace(transform, new Rect (-1.5f, -1.5f, 3, 3)), "Slave");
			if(characters.Length == 0) return;

			Slave targetSlave = (Slave)characters[0];
			GameObject resourceObject = GameObject.Instantiate(pickupPrefab);
			resourceObject.GetComponent<SpriteAnimationManager>().SwitchAnimation(resourceType, true);
			targetSlave.AddResource(resourceObject);

			slave = null;
			resourceReady = false;
			respawnCounter = respawnDelay;
		}

		respawnCounter -= Time.deltaTime * 60f;
	}

	public bool HasResourceReady() {
		return resourceReady;
	}
}
