﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class TileMap {
	public enum TileType {
		Water = 0,
		Dirt = 1,
		Grass = 2,
		Bare = 3,
		Lava = 4,
		Special = 5,
	};

	private class SpriteDefinition {
		public Sprite baseSprite;
		// The index into the list is a four-bit autotile entry
		public Dictionary<TileType, List<Sprite>> transition;

		public SpriteDefinition(Sprite baseSprite) {
			this.baseSprite = baseSprite;
			transition = new Dictionary<TileType, List<Sprite>>();
		}

		public void AddTransition(TileType type, Sprite sprite) {
			if(!transition.ContainsKey(type)) {
				transition[type] = new List<Sprite>();
			}
			transition[type].Add(sprite);
		}
	};

	private struct ChunkCache {
		public int startX, startY;
		public Sprite rendered;
		public TileType[,] tileData;
	};

	private Dictionary<int, Dictionary<int, ChunkCache>> chunkCache = 
		new Dictionary<int, Dictionary<int, ChunkCache>>();

	private Texture2D spriteSheet;
	private Dictionary<TileType, SpriteDefinition> sprites;
	// The base sprite everything else layers on top of
	private Sprite waterSprite;
	private Sprite lavaSprite;
	private Sprite[] workarounds;
	private NoiseField noiseField;

	public List<Vector2> resourceSpawners = new List<Vector2>();
	public Vector2 resourceConverter = new Vector2();

	public TileMap(Texture2D spriteSheet, uint seed = 0) {
		this.noiseField = new NoiseField(seed);
		this.spriteSheet = spriteSheet;
		this.sprites = new Dictionary<TileType, SpriteDefinition>();
		waterSprite = CreateSprite(7, 1);
		lavaSprite = CreateSprite(4, 1);

		workarounds = new Sprite[] {
			CreateSprite(13, 2, 40),
			CreateSprite(13, 6, 40),
			CreateSprite(4, 20, 40),
			CreateSprite(19, 17, 40),
			CreateSprite(20, 14, 40),
			CreateSprite(13, 17, 40)
		};

		InitTileSprites(TileType.Dirt, 1, 1);
		InitTransition(TileType.Dirt, TileType.Water, 6, 0, 5, 3);
		InitTransition(TileType.Dirt, TileType.Lava, 3, 0, 3, 3);
		InitTileSprites(TileType.Grass, 1, 6);
		InitTransition(TileType.Grass, TileType.Water, 6, 11, 9, 11);
		InitTransition(TileType.Grass, TileType.Dirt, 9, 5, 8, 8);
		InitTransition(TileType.Grass, TileType.Lava, 2, 11, 0, 11);
		InitTileSprites(TileType.Bare, 27, 1);
		InitTransition(TileType.Bare, TileType.Water, 22, 8, 22, 11);
		InitTransition(TileType.Bare, TileType.Dirt, 19, 3, 18, 6);
		InitTransition(TileType.Bare, TileType.Grass, 16, 8, 14, 10);
		InitTransition(TileType.Bare, TileType.Lava, 16, 3, 16, 6);
	}
		
	private Sprite CreateSprite(int x, int y, int size = 20) {
		int xPixel = x * 20;
		int yPixel = spriteSheet.height - (y * 20) - 20;
		return Sprite.Create(spriteSheet, new Rect(xPixel, yPixel, size, size), new Vector2(size/2, size/2));
	}

	private void InitTileSprites(TileType type, int baseX, int baseY) {
		sprites[type] = new SpriteDefinition(CreateSprite(baseX, baseY));
	}

	private void InitTransition(TileType above, TileType below, int concaveX, int concaveY, int convexX, int convexY) {
		SpriteDefinition def = sprites[above];
		def.AddTransition(below, null); // TODO: shouldn't happen?
		def.AddTransition(below, CreateSprite(concaveX + 0, concaveY + 0));
		def.AddTransition(below, CreateSprite(concaveX + 2, concaveY + 0));
		def.AddTransition(below, CreateSprite(concaveX + 1, concaveY + 0));
		def.AddTransition(below, CreateSprite(concaveX + 0, concaveY + 2));
		def.AddTransition(below, CreateSprite(concaveX + 0, concaveY + 1));
		def.AddTransition(below, null); // stupid diagonal
		def.AddTransition(below, CreateSprite(convexX  + 0, convexY  + 0));
		def.AddTransition(below, CreateSprite(concaveX + 2, concaveY + 2));
		def.AddTransition(below, null); // stupid diagonal
		def.AddTransition(below, CreateSprite(concaveX + 2, concaveY + 1));
		def.AddTransition(below, CreateSprite(convexX  + 1, convexY  + 0));
		def.AddTransition(below, CreateSprite(concaveX + 1, concaveY + 2));
		def.AddTransition(below, CreateSprite(convexX  + 0, convexY  + 1));
		def.AddTransition(below, CreateSprite(convexX  + 1, convexY  + 1));
		def.AddTransition(below, null); // TODO: shouldn't happen?
	}

	private ChunkCache ChunkAt(int aroundX, int aroundY) {
		int offsetX = aroundX % 100;
		if(offsetX < 0) offsetX += 100;
		int offsetY = aroundY % 100;
		if(offsetY < 0) offsetY += 100;

		int startX = aroundX - offsetX;
		int startY = aroundY - offsetY;

		Dictionary<int, ChunkCache> row;
		if(chunkCache.ContainsKey(startY)) {
			row = chunkCache[startY];
		} else {
			row = new Dictionary<int, ChunkCache>();
			chunkCache[startY] = row;
		}

		ChunkCache chunk;
		if(row.ContainsKey(startX)) {
			chunk = row[startX];
		} else {
			chunk = new ChunkCache();
			chunk.startX = startX;
			chunk.startY = startY;
			chunk.tileData = new TileType[100, 100];
			PopulateChunk(chunk);
			row[startX] = chunk;
		}

		return chunk;
	}

	private static TileType TypeForValue(double value) {
		//if(value < 0) return TileType.Water; else return TileType.Dirt;
		if(value < -75) {
			return TileType.Water;
		} else if(value < -50) {
			return TileType.Dirt;
		} else if(value < 50) {
			return TileType.Grass;
		} else if(value < 75) {
			return TileType.Bare;
		} else {
			return TileType.Lava;
		}
	}

	private void PopulateChunk(ChunkCache chunk) {
		double value;
		for(int y = 0; y < 100; y += 2) {
			for(int x = 0; x < 100; x += 2) {
				value = noiseField.ValueAt(x + chunk.startX, y + chunk.startY);
				TileType type = TypeForValue(value);
				chunk.tileData[x, y] = type;
				chunk.tileData[x+1, y] = type;
				chunk.tileData[x, y+1] = type;
				chunk.tileData[x+1, y+1] = type;
			}
		}
	}

	/// <summary>Returns the type of the tile at the specified coordinates.</summary>
	/// <param name="x">The x coordinate</param>
	/// <param name="y">The y coordinate</param>
	/// <returns>One of the possible tile types</returns>
	public TileType GetTileType(int x, int y) {
		ChunkCache chunk = ChunkAt(x, y);
		return chunk.tileData[x - chunk.startX, y - chunk.startY];
	}

	/// <summary>Returns the type of the tile at the specified coordinates.</summary>
	/// <param name="pos">The coordinates as a vector</param>
	/// <returns>One of the possible tile types</returns>
	public TileType GetTileType(Vector2 pos) {
		return GetTileType((int)pos.x, (int)pos.y);
	}

	/// <summary>Returns whether the tile at the specified coordinates can be walked on.</summary>
	/// <param name="x">The x coordinate</param>
	/// <param name="y">The y coordinate</param>
	/// <returns>True if the tile can be walked on</returns>
	public bool IsWalkable(int x, int y) {
		if(x < 0 || x > 99 || y < 0 || y > 99) return false;
		TileType tile = GetTileType(x, y);
		return tile != TileType.Water && tile != TileType.Lava && tile != TileType.Special;
	}

	/// <summary>Returns the graphical representation of the specified tile.</summary>
	/// <param name="x">The x coordinate</param>
	/// <param name="y">The y coordinate</param>
	/// <returns>A Sprite representing the texture of the tile</returns>
	public Sprite TileAt(int x, int y) {
		TileType type = GetTileType(x, y);
		switch(type) {
		case TileType.Water:
			return waterSprite;
		case TileType.Lava:
			return lavaSprite;
		default:
			return sprites[type].baseSprite;
		}
	}

	private static TileType BestEdge(TileType type, TileType[] tiles) {
		Dictionary<TileType, int> counts = new Dictionary<TileType, int>();
		for(int i = 0; i < tiles.Length; i++) {
			int incr = (tiles[i] == type) ? 0 : 1;
			if(counts.ContainsKey(tiles[i])) {
				counts[tiles[i]] += incr;
			} else {
				counts[tiles[i]] = incr;
			}
		}
		TileType best = tiles[0];
		int bestCount = counts[tiles[0]];
		for(int i = 1; i < tiles.Length; i++) {
			if(counts[tiles[i]] > bestCount) {
				bestCount = counts[tiles[i]];
				best = tiles[i];
			}
		}
		if(bestCount == 0) {
			return type;
		} else {
			return best;
		}
	}

	private static Sprite GetAutoTile(SpriteDefinition def, TileType[,] range,
		int b0x, int b0y,
		int b1x, int b1y,
		int b2x, int b2y,
		int b3x, int b3y) {
		TileType other = BestEdge(
			range[1, 1], 
			new TileType[] {
				range[b0x, b0y],
				range[b1x, b1y],
				range[b2x, b2y],
				range[b3x, b3y]
			});
		int bits =
			((other == range[b0x, b0y])   ? 1 : 0) |
			((other == range[b1x, b1y])   ? 2 : 0) |
			((other == range[b2x, b2y])   ? 4 : 0) |
			((other == range[b3x, b3y])   ? 8 : 0);
		if(bits != 0 && def.transition.ContainsKey(other)) {
			return def.transition[other][bits];
		} else {
			return def.baseSprite;
		}
	}

	private void ApplyAutoTiles(Sprite[,] map, int startX, int startY, TileType type) {
		SpriteDefinition def = sprites[type];
		TileType[,] range = new TileType[6, 6];
		int cx, cy;
		for(int y = 2; y < 102; y += 2) {
			for(int x = 2; x < 102; x += 2) {
				cx = startX + x;
				cy = startY + y;
				if(GetTileType(cx - 2, cy - 2) != type) {
					continue;
				}
				for(int v = 0; v < 4; v++) {
					for(int u = 0; u < 4; u++) {
						range[u, v] = GetTileType(cx + u - 3, cy + v - 3);
					}
				}

				map[x-1, y-1] = GetAutoTile(
					def, range,
					1, 1,
					0, 1,
					1, 0,
					0, 0
				);
				map[x-1, y] = GetAutoTile(
					def, range,
					1, 1,
					0, 1,
					1, 2,
					0, 2
				);
				map[x-1, y+1] = GetAutoTile(
					def, range,
					1, 3,
					0, 3,
					1, 2,
					0, 2
				);
				map[x, y-1] = GetAutoTile(
					def, range,
					1, 1,
					2, 1,
					1, 0,
					2, 0
				);
				map[x, y] = def.baseSprite;
				map[x, y+1] = GetAutoTile(
					def, range,
					1, 3,
					2, 3,
					1, 2,
					2, 2
				);

				map[x+1, y-1] = GetAutoTile(
					def, range,
					3, 1,
					2, 1,
					3, 0,
					2, 0
				);
				map[x+1, y] = GetAutoTile(
					def, range,
					3, 1,
					2, 1,
					3, 2,
					2, 2
				);
				map[x+1, y+1] = GetAutoTile(
					def, range,
					3, 3,
					2, 3,
					3, 2,
					2, 2
				);
			}
		}
	}

	private static void Blit(Texture2D target, int x, int y, Texture2D tileTexture, Rect tileRect) {
		int uMin = (int)(tileRect.xMin);
		int vMin = (int)(tileRect.yMin);
		int width = (int)tileRect.width;
		int height = (int)tileRect.height;
		for(int v = 0; v < height; v++) {
			for(int u = 0; u < width; u++) {
				target.SetPixel(x + u, y + v, tileTexture.GetPixel(uMin + u, vMin + v));
			}
		}
	}

	public Sprite RenderMap(int aroundX, int aroundY) {
		ChunkCache chunk = ChunkAt(aroundX, aroundY);
		if(chunk.rendered) {
			return chunk.rendered;
		}

		int startX = chunk.startX;
		int startY = chunk.startY;

		Texture2D target = new Texture2D(2000, 1960, TextureFormat.RGB24, false);
		Sprite[,] sprites = new Sprite[104, 104];
		for(int y = 0; y < 104; y++) {
			for(int x = 0; x < 104; x++) {
				TileType type = GetTileType(startX + x - 2, startY + y - 2);
				if(type == TileType.Water || type == TileType.Dirt || type == TileType.Grass) {
					sprites[x, y] = waterSprite;
				} else {
					sprites[x, y] = lavaSprite;
				}
			}
		}
		ApplyAutoTiles(sprites, startX, startY, TileType.Dirt);
		ApplyAutoTiles(sprites, startX, startY, TileType.Grass);
		ApplyAutoTiles(sprites, startX, startY, TileType.Bare);
		for(int y = 2; y < 100; y++) {
			for(int x = 2; x < 102; x++) {
				Sprite tile = sprites[x, 102 - y];
				if(tile == null) {
					if(101 - y < 98 && 101 - y >= 0) {
						chunk.tileData[x - 2, 101 - y] = TileType.Special;
						chunk.tileData[x - 1, 101 - y] = TileType.Special;
					}
					if(100 - y < 98 && 100 - y >= 0) {
						chunk.tileData[x - 2, 100 - y] = TileType.Special;
						chunk.tileData[x - 1, 100 - y] = TileType.Special;
					}
					resourceSpawners.Add(new Vector2(x - 1.5f, 101f - y));
					continue;
				}
				Texture2D tileTexture = tile.texture;
				Rect tileRect = tile.textureRect;
				Blit(target, x * 20 - 40, y * 20 - 40, tileTexture, tileRect);
			}
		}

		for(int y = 10; y < 90; y += 20) {
			for(int x = 10; x < 90; x += 20) {
				bool hasSpawner = false;
				Rect region = new Rect(x, y, 19, 19);
				foreach(Vector2 spawner in resourceSpawners) {
					if(region.Contains(spawner)) {
						hasSpawner = true;
						break;
					}
				}
				if(!hasSpawner) {
					Vector2 spawner = FindOpenTile(new Rect(x, 80 - y, 19, 19), new Vector2(), 0, 10);
					if(spawner.x >= 0 && spawner.y >= 0) {
						spawner.x = Mathf.Floor(spawner.x) - 0.5f;
						spawner.y = Mathf.Floor(spawner.y) - 1.0f;
						chunk.tileData[(int)spawner.x + 1, (int)spawner.y - 1] = TileType.Special;
						chunk.tileData[(int)spawner.x, (int)spawner.y - 1] = TileType.Special;
						chunk.tileData[(int)spawner.x + 1, (int)spawner.y] = TileType.Special;
						chunk.tileData[(int)spawner.x, (int)spawner.y] = TileType.Special;
						resourceSpawners.Add(spawner);
					}
				}
			}
		}

		resourceConverter = FindOpenTile(new Rect(20, 20, 60, 60));
		resourceConverter.x = Mathf.Floor(resourceConverter.x) - 0.5f;
		resourceConverter.y = Mathf.Floor(resourceConverter.y) - 1.0f;
		chunk.tileData[(int)resourceConverter.x + 1, (int)resourceConverter.y - 1] = TileType.Special;
		chunk.tileData[(int)resourceConverter.x, (int)resourceConverter.y - 1] = TileType.Special;
		chunk.tileData[(int)resourceConverter.x + 1, (int)resourceConverter.y] = TileType.Special;
		chunk.tileData[(int)resourceConverter.x, (int)resourceConverter.y] = TileType.Special;
		resourceSpawners.Add(resourceConverter);

		foreach(Vector2 spawner in resourceSpawners) {
			int x = Mathf.FloorToInt(spawner.x) + 2;
			int y = 101 - Mathf.FloorToInt(spawner.y);
			int number = (int)noiseField.ValueAt(startX + x, startY + y);
			if(number < 0) number = -number;
			Sprite tile = workarounds[number % workarounds.Length];
			Texture2D tileTexture = tile.texture;
			Rect tileRect = tile.textureRect;
			Blit(target, x * 20 - 50, y * 20 - 45, tileTexture, tileRect);
		}
		target.Apply();

		// Remove the converter and spawners too close to the edges
		resourceSpawners.Remove(resourceConverter);
		Rect safeRect = new Rect(2, 2, 96, 94);
		for(int i = resourceSpawners.Count - 1; i >= 0; --i) {
			Vector2 spawner = resourceSpawners[i];
			if(!safeRect.Contains(spawner)) {
				resourceSpawners.RemoveAt(i);
			}
		}

		chunk.rendered = Sprite.Create(target, new Rect(0, 0, 2000, 1960), new Vector2(0, 0), 20);
		return chunk.rendered;
	}

	public Vector2 FindOpenTile(Rect range, Vector2 avoid = new Vector2(), int avoidRange = 10, int maxAttempts = -1) {
		Vector2 spawnpoint = new Vector2();
		bool valid;
		if (range.xMin < 0) range.xMin = 0;
		if (range.xMax > 99) range.xMax = 99;
		if (range.yMin < 0) range.yMin = 0;
		if (range.yMax > 97) range.yMax = 97;
		do {
			spawnpoint.x = Random.Range(range.xMin, range.xMax);
			spawnpoint.y = Random.Range(range.yMin, range.yMax);
			if((spawnpoint - avoid).magnitude < avoidRange) {
				valid = false;
				continue;
			}
			valid = true;
			for(int y = (int)spawnpoint.y - 1; valid && y <= (int)spawnpoint.y + 1; y++) {
				for(int x = (int)spawnpoint.x - 1; valid && x <= (int)spawnpoint.x + 1; x++) {
					if(!IsWalkable(x, 100 - y)) {
						valid = false;
						Debug.Log("invalid spawnpoint, retrying");
					}
				}
			}
			if(maxAttempts > 0) {
				--maxAttempts;
				if(maxAttempts < 1) return new Vector2(-1, -1);
			}
		} while(!valid);
		return spawnpoint;
	}
}
