﻿using UnityEngine;
using System.Collections;

public class PlayerControl : CharacterCore {
	public static int score = 0;
	public static int killsSinceLastDamage = 0;
	private float attackDuration = 0;
	private float walkSoundTime = 0;
	public static bool paused = false;

	public override void Start() {
		base.Start();
		baseColor = new Color(0.2f, 0.8f, 0.8f);
		GetComponent<SpriteRenderer>().color = baseColor;
		myDamageSound = playerDamageSound;
		myDeathSound = playerDeathSound;
		score = 0;
		killsSinceLastDamage = 0;
		paused = false;
	}

	public override void Update() {
		base.Update();
		if (Input.GetButtonDown ("Cancel") || (paused && Input.anyKeyDown))
			paused = !paused;

		if (!paused) {
			Time.timeScale = 1;
			Vector2 input = Vector2.ClampMagnitude (new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical")), 1);
			float inputX = input.x;
			float inputY = input.y;

			MovementControl (inputX, inputY);
			if ((inputX != 0 || inputY != 0) && knockbackForce <= 0.5f && walkSoundTime <= 0) {
				TileMap.TileType tile = map.GetTileType ((int)transform.position.x, 100 - (int)transform.position.y);
				if (tile == TileMap.TileType.Bare || tile == TileMap.TileType.Lava) {
					PlaySound (walkSound, 2f, 3f);
				} else if (tile == TileMap.TileType.Dirt || tile == TileMap.TileType.Water) {
					PlaySound (walkSound, 0.25f, 0.3f);
				} else {
					PlaySound (walkSound);
				}
				walkSoundTime = 0.2f;
			} else if (walkSoundTime > 0) {
				walkSoundTime -= Time.deltaTime;
				if (walkSoundTime < 0)
					walkSoundTime = 0;
			}

			SpriteAnimationManager manager = GetComponent<SpriteAnimationManager> ();
			if (Input.GetButtonDown ("Jump") && damageAnimation <= 0 && !manager.OneShotPlaying ()) {
				AnimatePunch(inputX, inputY);
				attackDuration = 0.134f; // ~8 frames
			}

			if (attackDuration > 0) {
				Rect attackBox = RectToWorldSpace (transform, hitBox);
				attackDuration -= Time.deltaTime;

				CharacterCore[] characters = CollisionCheck (attackBox, "Enemy");
				if (characters == null)
					return;

				foreach (CharacterCore character in characters) {
					character.ApplyKnockback (15, transform.position);
					character.ApplyDamage (15);
				}
			}
		} else {
			Time.timeScale = 0;
		}
	}

	protected override void OnZeroHealth()
	{
		base.OnZeroHealth();
		//LOADING
		int[] scores = new int[10];
		for(int i=0; i<10; i++) 
		{
			scores [i] = PlayerPrefs.GetInt ("score " + i, -1);
		}

		int score_ = PlayerControl.score;

		if (score_ > 0) {
			if (score_ > scores [9])
				scores [9] = score_;
		
			for (int i = 8; i >= 0; i--) {
				if (scores [i + 1] > scores [i]) {
					int tmp = scores [i];
					scores [i] = scores [i + 1];
					scores [i + 1] = tmp;
				}
			}

			for (int i = 0; i < 10; i++) {
				PlayerPrefs.SetInt ("score " + i, scores [i]);
			}
		}

		CameraControl.showScores = true;
	}

	public override void OnDamageTaken() {
		killsSinceLastDamage = 0;
	}

	public static void IncreaseScore(int amount)
	{
		Object[] slaves = GameObject.FindGameObjectsWithTag ("Slave");
		score += ((amount + (10 * slaves.Length)) * (killsSinceLastDamage + 1));
	}


}
