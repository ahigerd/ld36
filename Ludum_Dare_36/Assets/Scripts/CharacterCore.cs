﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterCore : MonoBehaviour {

	public float health = 100;
	public float maxHealth = 100;
	private float deathTime = 0;
	//[Range(0.0f,1.0f)]
	public float adjustmentPercent = 0.5f;

	public float maxSpeed = 4;
	public Rect hitBox = new Rect (0, 0, 1, 1);

	protected float currentSpeedX = 0;
	protected float currentSpeedY = 0;

	public static TileMap map;

	protected Vector3 knockbackDirection;
	protected float knockbackForce = 0;

	protected Texture2D healthTexture;
	protected Sprite healthSprite;
	protected SpriteRenderer healthRenderer;

	protected Color baseColor;
	protected float damageAnimation = 0;
	protected float healAnimation = 0;

	protected static AudioClip questionSound, confirmSound, finishedSound, damageSound, walkSound;
	protected static AudioClip minionDamageSound, playerDamageSound, punchSound, pickupSound;
	protected static AudioClip enemyDeathSound, minionDeathSound, playerDeathSound, healSound;
	protected AudioClip myDamageSound, myDeathSound;
	protected AudioSource audioSource;

	public GameObject damageArrow;

	public static List<CharacterCore> allCharacters = new List<CharacterCore>();

	public bool InPain() {
		return deathTime > 0 || damageAnimation > 0;
	}

	public virtual void OnDestroy() {
		allCharacters.Remove(this);
		if (damageArrow != null) {
			GameObject.Destroy(damageArrow);
		}
	}

	public void MovementControl(Vector3 vector) {
		MovementControl(vector.x, vector.y);
	}

	public void MovementControl(Vector2 vector) {
		MovementControl(vector.x, vector.y);
	}

	public static bool TileIsWalkable(float x, float y) {
		return map.IsWalkable((int)(x + 0.5), 100 - (int)(y + 0.7));
	}

	public static bool TileIsWalkable(Vector3 position) {
		return TileIsWalkable(position.x, position.y);
	}

	public void AnimatePunch (float inputX, float inputY) {
		SpriteAnimationManager manager = GetComponent<SpriteAnimationManager> ();
		if (inputX == 0 && inputY == 0) {
			manager.PlayOnce ("FrontPunch");
		} else if (Mathf.Abs (inputY) > Mathf.Abs (inputX)) {
			if (inputY < 0) {
				manager.PlayOnce ("FrontPunch");
			} else {
				manager.PlayOnce ("BackPunch");
			}
		} else {
			manager.PlayOnce ("SidePunch");
		}
		PlaySound (punchSound);
	}

	//  Called in Update. Requires Time.deltaTime
	public void MovementControl (float inputX, float inputY) {
		if (deathTime > 0) {
			inputX = 0;
			inputY = 0;
		}

		SpriteAnimationManager manager = GetComponent<SpriteAnimationManager> ();
		if (inputX == 0 && inputY == 0) {
			manager.SwitchAnimation ("Idle", true);
		} else if (Mathf.Abs (inputY) > Mathf.Abs (inputX)) {
			if (inputY < 0) {
				manager.SwitchAnimation ("FrontWalking", true);
			} else {
				manager.SwitchAnimation ("BackWalking", true);
			}
		} else {
			manager.SwitchAnimation ("SideWalking", true);
		}

		if (knockbackForce > 1.5f) {
			knockbackForce = Approach(knockbackForce, 0, 0.1f);
			currentSpeedX = knockbackDirection.x * knockbackForce;
			currentSpeedY = knockbackDirection.y * knockbackForce;
		} else {
			knockbackForce = 0;
		}
		currentSpeedX = (currentSpeedX + Approach (currentSpeedX, maxSpeed * inputX, adjustmentPercent)) /2;
		currentSpeedY = (currentSpeedY + Approach (currentSpeedY, maxSpeed * inputY, adjustmentPercent)) /2;

		if (inputX < 0) {
			GetComponent<SpriteRenderer> ().flipX = true;
		} else if (inputX > 0) {
			GetComponent<SpriteRenderer> ().flipX = false;
		}

		Vector3 newPosition = new Vector3 (
			                      Mathf.Clamp (transform.position.x + Time.deltaTime * currentSpeedX, 0.5f, 99.5f),
			                      Mathf.Clamp (transform.position.y + Time.deltaTime * currentSpeedY, 0.7f, 97.5f),
			                      transform.position.z);
		Vector3 finalPosition = transform.position;

		//bool isStuck = !map.IsWalkable((int)(transform.position.x + 0.5), 100 - (int)(transform.position.y + 0.7));
		bool isStuck = !TileIsWalkable(transform.position);
		bool collided = false;

		float newKnockbackDirectionX = knockbackDirection.x;
		for (float t = 1.0f; t >= 0.0f; t -= 0.1f) {
			float attemptX = Mathf.Lerp (transform.position.x, newPosition.x, t);
			//if (isStuck || map.IsWalkable ((int)(attemptX + 0.5), 100 - (int)(transform.position.y + 0.7))) {
			if (isStuck || TileIsWalkable(attemptX, transform.position.y)) {
				finalPosition.x = attemptX;
				knockbackDirection.x = newKnockbackDirectionX;
				break;
			}

			newKnockbackDirectionX = -knockbackDirection.x / 2;
			collided = true;
		}

		float newKnockbackDirectionY = knockbackDirection.y;
		for (float t = 1.0f; t >= 0.0f; t -= 0.1f) {
			float attemptY = Mathf.Lerp (transform.position.y, newPosition.y, t);
			//if (isStuck || map.IsWalkable ((int)(finalPosition.x + 0.5), 100 - (int)(attemptY + 0.7))) {
			if (isStuck || TileIsWalkable(finalPosition.x, attemptY)) {
				finalPosition.y = attemptY;
				knockbackDirection.y = newKnockbackDirectionY;
				break;
			}
			newKnockbackDirectionY = -knockbackDirection.y / 2;
			collided = true;
		}

		if (collided)
			OnMapCollisionTriggered ();

		transform.position = finalPosition;
	}

	protected virtual void OnZeroHealth() {
		PlaySound(myDeathSound);
		deathTime = myDeathSound.length;
	}

	protected virtual void OnMapCollisionTriggered() {
	}

	public static Color ColorForHealth(float healthFraction) {
		float hue = healthFraction / 3.0f;
		return Color.HSVToRGB(hue, 1.0f, 1.0f);
	}

	protected void PlaySound(AudioClip clip, float minPitch = 0.85f, float maxPitch = 1.2f) {
		audioSource.pitch = Random.Range(minPitch, maxPitch);
		audioSource.PlayOneShot(clip);
	}

	public virtual void Start () {
		if(questionSound == null) {
			questionSound     = Resources.Load("Sounds/question") as AudioClip;
			confirmSound      = Resources.Load("Sounds/confirm") as AudioClip;
			finishedSound     = Resources.Load("Sounds/finished") as AudioClip;
			damageSound       = Resources.Load("Sounds/damage") as AudioClip;
			minionDamageSound = Resources.Load("Sounds/miniondamage") as AudioClip;
			playerDamageSound = Resources.Load("Sounds/playerdamage") as AudioClip;
			punchSound        = Resources.Load("Sounds/punch") as AudioClip;
			enemyDeathSound   = Resources.Load("Sounds/enemydeath") as AudioClip;
			minionDeathSound  = Resources.Load("Sounds/miniondeath") as AudioClip;
			playerDeathSound  = Resources.Load("Sounds/playerdeath") as AudioClip;
			walkSound         = Resources.Load("Sounds/walking") as AudioClip;
			pickupSound       = Resources.Load("Sounds/pickup") as AudioClip;
			healSound         = Resources.Load("Sounds/heal") as AudioClip;
		}

		allCharacters.Add(this);

		myDamageSound = damageSound; // can be overridden in subclasses
		myDeathSound = enemyDeathSound;
		audioSource = gameObject.AddComponent<AudioSource>();
		audioSource.volume = 0.7f;

		baseColor = GetComponent<SpriteRenderer>().color;

		health = maxHealth;
		healthTexture = new Texture2D(40, 5, TextureFormat.RGB24, false);
		for(int y = 0; y < 5; y++) {
			for(int x = 0; x < 40; x++) {
				healthTexture.SetPixel(x, y, Color.white);
			}
		}
		GameObject healthObject = new GameObject("healthbar");
		healthRenderer = healthObject.AddComponent<SpriteRenderer>();
		healthRenderer.sortingOrder = 32766;
		healthRenderer.transform.position = new Vector2(0, 0.8f);
		healthRenderer.transform.SetParent(transform, false);
		healthObject.SetActive(true);
		UpdateHealthBar();
	}

	protected void UpdateHealthBar() {
		float healthFraction = health / maxHealth;
		int healthX = Mathf.CeilToInt(healthFraction * 38) + 1;
		for(int x = 1; x < 39; x++) {
			Color color = (x <= healthX && deathTime == 0) ? ColorForHealth(healthFraction) : Color.black;
			for(int y = 1; y < 4; y++) {
				healthTexture.SetPixel(x, y, color);
			}
		}
		healthTexture.Apply();
		if(healthSprite != null) {
			Object.Destroy(healthSprite);
		}
		healthSprite = Sprite.Create(
			healthTexture, 
			new Rect(0, 0, 40, 5), 
			new Vector2(0.5f, 0),
			40);
		healthRenderer.sprite = healthSprite;
		healthRenderer.enabled = (health < maxHealth);
	}

	public static CharacterCore[] CollisionCheck (Rect rect, string tag)
	{
		List<CharacterCore> characters = new List<CharacterCore> ();
		GameObject[] objects = GameObject.FindGameObjectsWithTag (tag);
		foreach (GameObject obj in objects) {
			CharacterCore core = obj.GetComponent<CharacterCore> ();
			if(core == null) continue;

			Rect testRect = RectToWorldSpace(core.transform, core.hitBox);
			//Debug.Log ("Input Rect: " + rect + ", Test Rect: " + testRect);
			if (rect.Overlaps(testRect)) {
				characters.Add (core);
			}
		}

		return characters.ToArray();
	}

	public Vector3 GetDirectionTo(Vector3 position)
	{
		return (position - transform.position).normalized;
	}

	public void ApplyKnockback( float amount, Vector3 position, bool unconditional = false)
	{
		if(!unconditional && (damageAnimation > 0 || deathTime > 0)) {
			// Can't take damage while blinking
			return;
		}
		knockbackDirection = -GetDirectionTo (position);
		knockbackForce = amount;
	}

	public virtual void OnDamageTaken() {
	}

	public void ApplyDamage (float damage) {
		if(damageAnimation > 0 || deathTime > 0) {
			// Can't take damage while blinking
			return;
		}
		OnDamageTaken ();
		health = Mathf.Clamp(health - damage, 0, 100);
		damageAnimation = 0.5f;

		UpdateHealthBar();

		if (health == 0) {
			OnZeroHealth ();
		} else {
			PlaySound(myDamageSound);
		}
	}

	public virtual void OnHealthRestore() {
	}

	public void RestoreHealth (int amount) {
		OnHealthRestore ();
		health = Mathf.Clamp(health + amount, 0, 100);
		UpdateHealthBar();
		audioSource.pitch = (this is Slave) ? Random.Range(2.2f, 2.5f) : Random.Range(0.8f, 1.1f);
		audioSource.PlayOneShot(healSound);
		healAnimation = 0.5f;
	}

	public static float Approach(float current, float target, float rate, float time = -1)
	{
		if(Mathf.Abs(current - target) < 0.1f) return target;
		if(time < 0) time = Time.deltaTime;
		float steps = time * 60; // scale for a base rate of 60fps
		return (current - target) * Mathf.Pow(0.5f, rate * steps) + target;
	}

	// Vector3 parameters in world space
	public static void DrawDebugRect(Vector3 bottomLeft, Vector3 topRight)
	{
		Vector3 topLeft = new Vector3(bottomLeft.x, topRight.y, 0);
		Vector3 bottomRight = new Vector3(topRight.x, bottomLeft.y, 0);
		Debug.DrawLine(bottomLeft, bottomRight, Color.red, 0, false);
		Debug.DrawLine(bottomRight, topRight, Color.green, 0, false);
		Debug.DrawLine(topRight, topLeft, Color.blue, 0, false);
		Debug.DrawLine(topLeft, bottomLeft, Color.yellow, 0, false);
	}

	// Vector2 parameters in world space
	public static void DrawDebugRect(Vector2 bottomLeft, Vector2 topRight)
	{
		Vector3 bottomLeft3D = new Vector3(bottomLeft.x, bottomLeft.y, 0);
		Vector3 topRight3D = new Vector3(topRight.x, topRight.y, 0);
		DrawDebugRect(bottomLeft3D, topRight3D);
	}

	// Rect in world space
	public static void DrawDebugRect(Rect worldRect)
	{
		DrawDebugRect(worldRect.min, worldRect.max);
	}

	// Rect in transform-local space
	public static void DrawDebugRect(Transform transform, Rect rect)
	{
		DrawDebugRect(RectToWorldSpace(transform, rect));
	}

	public static Rect RectToWorldSpace(Transform transform, Rect rect)
	{
		Vector3 bottomLeft = transform.TransformPoint(new Vector3(rect.xMin, rect.yMin, 0));
		Vector3 size = transform.TransformPoint(new Vector3(rect.xMax, rect.yMax, 0)) - bottomLeft;
		return new Rect(bottomLeft.x, bottomLeft.y, size.x, size.y);
	}

	public void OnDrawGizmos()
	{
		DrawDebugRect(transform, hitBox);
	}

	public virtual void Update()
	{
		SpriteRenderer sr = GetComponent<SpriteRenderer>();
		if(deathTime > 0) {
			deathTime -= Time.deltaTime;
			float deathFraction = deathTime / myDeathSound.length;
			float gray = (1.0f - deathFraction) / 2.0f;
			sr.color = new Color(gray, gray, gray, deathFraction);
			if(deathTime <= 0) {
				GameObject.Destroy (gameObject);
			}
		} else if(damageAnimation != 0.0f) {
			damageAnimation -= Time.deltaTime;
			int frame = Mathf.FloorToInt(damageAnimation * 1000);
			if(damageAnimation > 0 && frame % 100 < 50) {
				sr.color = Color.black;
			} else {
				sr.color = baseColor;
			}
			if(damageAnimation <= 0)
				damageAnimation = 0;
		} else if(healAnimation != 0.0f) {
			sr.color = new Color(1.0f - healAnimation * 2.0f, 1, 1.0f - healAnimation * 2.0f, 1);
			healAnimation -= Time.deltaTime;
			if(healAnimation <= 0) {
				healAnimation = 0;
				sr.color = baseColor;
			}
		} else {
			sr.color = baseColor;
		}
	}
}
