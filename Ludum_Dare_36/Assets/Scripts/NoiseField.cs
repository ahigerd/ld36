﻿using UnityEngine;
using System;
using System.Collections;

public class NoiseField {
	private static double Dot(int[] g, double x, double y) {
		return g[0] * x + g[1] * y; 
	}

	public static uint Hash(int x, int y, uint seedToUse)
	{
		// based on Jenkins96 hash
		uint a = (uint)(0x9e3779b9 + x);
		uint b = (uint)(0x9e3779b9 + y);
		uint c = seedToUse;

		a -= b; a -= c; a ^= (c>>13); 
		b -= c; b -= a; b ^= (a<<8); 
		c -= a; c -= b; c ^= (b>>13); 
		a -= b; a -= c; a ^= (c>>12);  
		b -= c; b -= a; b ^= (a<<16); 
		c -= a; c -= b; c ^= (b>>5); 
		a -= b; a -= c; a ^= (c>>3);  
		b -= c; b -= a; b ^= (a<<10); 
		c -= a; c -= b; c ^= (b>>15); 

		c += 12;

		a -= b; a -= c; a ^= (c>>13);
		b -= c; b -= a; b ^= (a<<8);
		c -= a; c -= b; c ^= (b>>13);
		a -= b; a -= c; a ^= (c>>12);
		b -= c; b -= a; b ^= (a<<16);
		c -= a; c -= b; c ^= (b>>5);
		a -= b; a -= c; a ^= (c>>3);
		b -= c; b -= a; b ^= (a<<10);
		c -= a; c -= b; c ^= (b>>15);

		return c & 0xFFFFFFFF;
	}

	private static readonly int[][] grad3 = {
		new int[] { 1, 1, 0},
		new int[] {-1, 1, 0},
		new int[] { 1,-1, 0},
		new int[] {-1,-1, 0},
		new int[] { 1, 0, 1},
		new int[] {-1, 0, 1},
		new int[] { 1, 0,-1},
		new int[] {-1, 0,-1},
		new int[] { 0, 1, 1},
		new int[] { 0,-1, 1},
		new int[] { 0, 1,-1},
		new int[] { 0,-1,-1}
	};

	public static double noise(double xin, double yin, uint seed, double scale = 1.0) {
		xin /= scale;
		yin /= scale;
		double n0, n1, n2;

		//skew input space
		double F2 = 0.5 * (Math.Sqrt(3.0) - 1.0);
		double s = (xin + yin) * F2;
		int i = (int)Math.Floor(xin + s);
		int j = (int)Math.Floor(yin + s);
		double G2 = (3.0 - Math.Sqrt(3.0)) / 6.0;
		double t = (i + j) * G2;

		// unskew
		double X0 = i - t;
		double Y0 = j - t;
		// x,y distance from cell origin
		double x0 = xin - X0; 
		double y0 = yin - Y0;

		// offsets for middle corner
		int i1, j1;
		if(x0 > y0) {
			// lower triangle order: (0,0)->(1,0)->(1,1)
			i1 = 1;
			j1 = 0;
		} else {
			//upper triangle order: (0,0)->(0,1)->(1,1)
			i1 = 0;
			j1 = 1;
		} 

		// middle corner offsets unskewed
		double x1 = x0 - i1 + G2; 
		double y1 = y0 - j1 + G2;
		//last corner offsets unskewed
		double x2 = x0 - 1.0 + 2.0 * G2; 
		double y2 = y0 - 1.0 + 2.0 * G2;

		// generate pseudorandom values for corners
		uint gi0 = Hash(i, j, seed) % 12;
		uint gi1 = Hash(i+i1, j+j1, seed) % 12;
		uint gi2 = Hash(i+1, j+1, seed) % 12;

		// calculate contributions from 3 corners
		double t0 = 0.5 - x0 * x0 - y0 * y0;
		if(t0 < 0) {
			n0 = 0.0;
		} else {
			t0 *= t0;
			n0 = t0 * t0 * Dot(grad3[gi0], x0, y0);
		}

		double t1 = 0.5 - x1 * x1 - y1 * y1;
		if(t1 < 0) {
			n1 = 0.0;
		} else {
			t1 *= t1;
			n1 = t1 * t1 * Dot(grad3[gi1], x1, y1);
		}

		double t2 = 0.5 - x2 * x2 - y2 * y2;
		if(t2 < 0) {
			n2 = 0.0;
		} else {
			t2 *= t2;
			n2 = t2 * t2 * Dot(grad3[gi2], x2, y2);
		}

		return n0 + n1 + n2;
	}

	private uint seed;
	private int octaves;
	private double scale, depth;

	public NoiseField(uint seed, int octaves = 1, double scale = 32.0, double depth = 7000.0) {
		this.seed = seed;
		this.octaves = octaves;
		this.scale = scale;
		this.depth = depth;
	}

	public double ValueAt(double x, double y) {
		double result = 0;
		double oScale = scale;
		double oDepth = depth;
		for(uint i = 0; i < octaves; i++) {
			result += noise(x, y, seed + i, oScale) * oDepth;
			oScale *= 2;
			oDepth /= 2;
		}
		return result;
	}
}
