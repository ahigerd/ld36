﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SpAnimation {
	public string name = "";
	public Texture2D sheet;
	public int rows;
	public int columns;
	public int frames;
	public float frameMS = 16.0f;
	public float duration;
	private Sprite[] sprites;

	public void Initialize(int pixelsPerUnit)
	{
		frames = rows * columns;
		duration = frameMS * frames;
		sprites = new Sprite[frames];
		int index = 0;
		for(int y = 0; y < rows; y++) {
			for(int x = 0; x < columns; x++) {
				Rect frameRect = GetFrameRect(index);
				sprites[index] = Sprite.Create(
					sheet,
					frameRect,
					new Vector2(0.5f, 0.5f),
					pixelsPerUnit);
				index++;
			}
		}
	}

	public Rect GetFrameRect (int frame)
	{
		int frameHeight = sheet.height / rows;
		int frameWidth = sheet.width / columns;

		int x = (frame % columns) * frameWidth;
		int y = sheet.height - (frame / columns + 1) * frameHeight;

		return new Rect (x, y, frameWidth, frameHeight);
	}

	public Sprite GetSprite(float step)
	{
		return sprites[Mathf.Clamp(Mathf.FloorToInt(step / frameMS), 0, frames - 1)];
	}
}

public class SpriteAnimationManager : MonoBehaviour {

	public int pixelsPerUnit = 300;
	public SpAnimation[] animations;
	private float step = 0;
	private int currentAnimation = 0;
	private bool isPlaying = true;
	private SpAnimation oneShot = null;

	void Start() {
		foreach(SpAnimation anim in animations) {
			anim.Initialize(pixelsPerUnit);
		}
	}

	void Update() {
		if (oneShot != null) {
			GetComponent<SpriteRenderer>().sprite = oneShot.GetSprite(step);
			step += Time.deltaTime * 1000;
			if(step >= oneShot.duration) {
				oneShot = null;
				step = 0;
			} else {
				return;
			}
		}

		SpAnimation animation = animations[currentAnimation];
		GetComponent<SpriteRenderer>().sprite = animation.GetSprite(step);
		if (isPlaying) {
			step += Time.deltaTime * 1000;
			if (step >= animation.duration) {
				step -= animation.duration;
			}
		}
	}

	public void Play() {
		isPlaying = true;
	}

	public void Stop() {
		isPlaying = false;
		if (oneShot == null)
			step = 0;
	}

	public void SetFrame(int frame)	{
		step = Mathf.Clamp(frame, 0, animations [currentAnimation].frames);
	}

	public void PlayOnce(int animation) {
		oneShot = animations[animation];
		step = 0;
	}

	public bool OneShotPlaying() {
		return oneShot != null;
	}

	public void PlayOnce(string name) {
		for (int i = 0; i < animations.Length; i++) {
			if (name == animations [i].name) {
				PlayOnce(i);
				return;
			}
		}
	}

	public void SwitchAnimation(int animation, bool play = false) 
	{
		if (currentAnimation != animation) {
			currentAnimation = animation;
			if(oneShot == null)
				step = 0;
		}
		if (play) Play ();
	}

	public void SwitchAnimation(string name, bool play = false) {

		for (int i = 0; i < animations.Length; i++) {
			if (name == animations [i].name) {
				SwitchAnimation(i, play);
				return;
			}
		}
	}
}
