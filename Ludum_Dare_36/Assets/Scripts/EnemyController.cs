﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyController : CharacterCore {

	private int delay = 0;
	public int maxAttackWait = 180;
	public int minAttackWait = 120;
	public static int killCounter = 1;

	protected override void OnZeroHealth() {
		base.OnZeroHealth ();
		PlayerControl.IncreaseScore (100);
		killCounter++;
	}

	void FixedUpdate()
	{
		delay--;
		List<GameObject> targets = new List<GameObject>(GameObject.FindGameObjectsWithTag("Slave"));
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		if (player != null) {
			targets.Add(player);
		}

		if (targets.Count == 0) {
			return;
		}

		GameObject closestTarget = player;
		float playerDistance = (player == null) ? 
			float.PositiveInfinity : 
			Vector3.Distance(player.transform.position, transform.position);
		float closestDistance = float.PositiveInfinity;
		foreach(GameObject target in targets) {
			float distance = Vector3.Distance(target.transform.position, transform.position);
			if(distance < closestDistance) {
				closestDistance = distance;
				closestTarget = target;
			}
		}

		Vector3 dir = Vector3.zero;
		if (playerDistance > 0.2f && playerDistance < 7) {
			// Chase the player if it's within range
			dir = GetDirectionTo(player.transform.position);
		} else if (closestTarget != null && closestDistance > 0.2f) {
			// Otherwise chase anything nearby
			dir = GetDirectionTo(closestTarget.transform.position);
		}

		if(closestDistance < 0.3f && delay <= 0 && damageAnimation <= 0) {
			// Something is close enough to hit. Take a swing!
			AnimatePunch (dir.x, dir.y);
			foreach(GameObject target in targets) {
				float distance = Vector3.Distance(target.transform.position, transform.position);
				if (distance < 0.5f) {
					target.GetComponent<CharacterCore>().ApplyKnockback (15, transform.position);
					target.GetComponent<CharacterCore>().ApplyDamage (5);
				}
			}
			delay = Random.Range (minAttackWait, maxAttackWait);
		}

		MovementControl (dir.x, dir.y);
	}

}
