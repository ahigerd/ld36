﻿using UnityEngine;
using System.Collections;

public class HomeBase : MonoBehaviour {

	public float shields = 100;

	// Use this for initialization
	public void InflictDamage(float damage)
	{
		shields -= damage;

		if (shields < 0)
			shields = 0; 
	}

	void FixedUpdate()
	{
		if (shields == 0) {
			//TODO impliment response to shields down
		}
	}
}
