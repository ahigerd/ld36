Ludum Dare 36
-------------
This is the Ludum Dare 36 jam submission for Adam Higerd, Austin Allman, and
Aaron Stouder.

Bobble Battle
============
Welcome to Bobble Battle! Protect your friends from your dark foes as they collect the crystals that keep you alive.

![Screenshot](preview.jpg)

Downloads
---------
* [Windows](https://bitbucket.org/ahigerd/ld36/downloads/Bobble_Battle_Win.zip)
* [Mac OS X](https://bitbucket.org/ahigerd/ld36/downloads/BobbleBattleOSX.zip)

Controls
--------
* Arrow keys / WASD: Move player
* Space: Attack
* Escape: Pause
* Left Click: Select a friend
* Right Click: Instruct friend to move

Credits
=======
Map tiles and pointer icon from "Hard Vacuum" by Daniel Cook (lostgarden.com)

Background music "Unearthly Chaos" by LacesLawless (http://www.newgrounds.com/audio/listen/541977)

License
=======
Copyright (c) 2016 Austin Allman, Adam Higerd, and Aaron Stouder

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
